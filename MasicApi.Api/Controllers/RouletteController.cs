﻿using AutoMapper;
using MasicApi.Core.Entities;
using MasicApi.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MasicApi.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RouletteController : Controller
    {
        private IRouletteRepository _rouletteRepository;
        private IMapper _mapper;
        public RouletteController(IRouletteRepository rouletteRepository,
            IMapper mapper)
        {
            _rouletteRepository = rouletteRepository;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("/api/roulette/create")]
        public IActionResult Create()
        {
            var roulette =  Task.FromResult(_rouletteRepository.Create());
            return new ObjectResult(roulette) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpPatch]
        [Route("/api/roulette/open")]
        public IActionResult Open(RouletteId rouletteId)
        {
            Roulette roulette = _mapper.Map<Roulette>(rouletteId);
            var rouletteResult = Task.FromResult(_rouletteRepository.Open(roulette));
            return new ObjectResult(rouletteResult) { StatusCode = StatusCodes.Status200OK };
        }

        [HttpPatch]
        [Route("/api/roulette/close")]
        public IActionResult Close(RouletteId rouletteId)
        {
            Roulette roulette = _mapper.Map<Roulette>(rouletteId);
            var bets = Task.FromResult(_rouletteRepository.Close(roulette));
            return new ObjectResult(bets) { StatusCode = StatusCodes.Status200OK };
        }

        [HttpGet]
        public IActionResult Get()
        {
            var roulettes = Task.FromResult(_rouletteRepository.Get());
            return new ObjectResult(roulettes) { StatusCode = StatusCodes.Status200OK };
        }
    }
}
