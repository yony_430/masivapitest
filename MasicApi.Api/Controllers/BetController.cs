﻿using MasicApi.Core.Entities;
using MasicApi.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MasicApi.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BetController : Controller
    {
        private IBetRepository _betRepository;
        public BetController(IBetRepository betRepository)
        {
            _betRepository = betRepository;
        }

        [HttpPost]
        [Route("/api/bet/create")]
        public IActionResult Create(Bet betData)
        {
            var bet = Task.FromResult(_betRepository.Create(betData));
            return Created("", bet);
        }
    }
}
