﻿using MasicApi.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MasicApi.Core.Interfaces
{
    public interface IFireStoreConnection
    {
        bool CreateRoulette(Roulette dataRoulette);
        Task<Roulette> GetByID(string id);
        bool UpdateRoulette(Roulette dataRoulette);
        bool CreateBet(Bet betData);
        Task<IEnumerable<Bet>> GetBets(Roulette dataRoulette);
        bool UpdateBet(Roulette dataRoulette, Bet dataBet);
        bool UpdateResultRoulette(Roulette dataRoulette);
        Task<IEnumerable<Roulette>> GetRoulettes();
    }
}
