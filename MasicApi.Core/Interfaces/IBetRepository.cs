﻿using MasicApi.Core.Entities;
using System.Threading.Tasks;

namespace MasicApi.Core.Interfaces
{
    public interface IBetRepository
    {
        Task<Bet> Create(Bet betData);
    }
}
