﻿using MasicApi.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MasicApi.Core.Interfaces
{
    public interface IRouletteRepository
    {
        Task<Roulette> Create();
        Task<Roulette> Open(Roulette roulette);
        Task<IEnumerable<Bet>> Close(Roulette roulette);
        Task<IEnumerable<Roulette>> Get();
    }
}
