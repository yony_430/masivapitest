﻿using System;
using System.Collections.Generic;

namespace MasicApi.Core.Entities
{
    public class Roulette
    {
        public string Id { get; set; }
        public string Status { get; set;}
        public string NumberWinner { get; set; }
        public string ColorWinner { get; set; }
        public IEnumerable<Bet> Bets { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
