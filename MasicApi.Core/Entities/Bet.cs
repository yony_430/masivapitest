﻿using System;

namespace MasicApi.Core.Entities
{
    public class Bet
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Number { get; set; }
        public string Color { get; set; }
        public double Amount { get; set; }
        public double TotalWinner { get; set; }
        public string RouletteId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
