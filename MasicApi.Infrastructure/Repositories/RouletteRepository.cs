﻿using MasicApi.Core.Entities;
using MasicApi.Core.Enums;
using MasicApi.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MasicApi.Infrastructure.Repositories
{
    public class RouletteRepository : IRouletteRepository
    {
        private IFireStoreConnection _fireStoreConnection;
        public RouletteRepository(IFireStoreConnection fireStoreConnection)
        {
            _fireStoreConnection = fireStoreConnection;
        }

        public Task<Roulette> Create()
        {
            Roulette rouletteData = new Roulette()
            {
                Id = Guid.NewGuid().ToString(),
                CreatedAt = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc),
                Status = Status.Created.ToString()
            };
            _fireStoreConnection.CreateRoulette(rouletteData);
            return Task.FromResult(rouletteData);
        }

        public async Task<Roulette> Open(Roulette roulette)
        {
            Roulette rouletteData = await _fireStoreConnection.GetByID(roulette.Id);
            if (rouletteData.Status == Status.Created.ToString())
            {
                roulette.Status = Status.Open.ToString();
                roulette.CreatedAt = rouletteData.CreatedAt;
                bool isUpdated = _fireStoreConnection.UpdateRoulette(roulette);
                if (isUpdated) {
                    return roulette;
                }
            }
            return null;
        }

        public async Task<IEnumerable<Bet>> Close(Roulette roulette)
        {
            Roulette rouletteData = await _fireStoreConnection.GetByID(roulette.Id);
            if (rouletteData != null && rouletteData.Status == Status.Open.ToString())
            {
                roulette.Status = Status.Close.ToString();
                bool isUpdated = _fireStoreConnection.UpdateRoulette(roulette);
                if (isUpdated)
                {
                    return await SetWinner(roulette);
                }
            }

            return null;
        }

        public async Task<IEnumerable<Roulette>> Get()
        {
            var roulettes = await _fireStoreConnection.GetRoulettes();
            foreach (Roulette roulette in roulettes)
            {
                roulette.Bets = await _fireStoreConnection.GetBets(roulette);
            }
            return roulettes;
        }

        private async Task<IEnumerable<Bet>> SetWinner(Roulette roulette)
        {
            int numberWinner = NumberWinner();
            Colors colorWinner = ColorWinner(numberWinner);
            IEnumerable<Bet> bets = await _fireStoreConnection.GetBets(roulette);

            foreach (Bet bet in bets)
            {
                double totalWinner = 0;
                if (bet.Number == numberWinner)
                {
                    totalWinner = bet.Amount * 5;
                }

                if (bet.Color == colorWinner.ToString())
                {
                    totalWinner += bet.Amount * 1.8;
                }

                if (totalWinner > 0)
                {
                    bet.TotalWinner = totalWinner;
                }

                _fireStoreConnection.UpdateBet(roulette, bet);

            }

            roulette.ColorWinner = colorWinner.ToString();
            roulette.NumberWinner = numberWinner.ToString();
            _fireStoreConnection.UpdateResultRoulette(roulette);
            return bets;
        }

        private int NumberWinner()
        {
            return new Random().Next(0, 36);
        }

        private Colors ColorWinner(int numberWinner)
        {
            return (numberWinner % 2 == 0) ? Colors.ROJO : Colors.NEGRO;
        }
    }
}
