﻿using MasicApi.Core.Entities;
using MasicApi.Core.Enums;
using MasicApi.Core.Interfaces;
using System;
using System.Threading.Tasks;

namespace MasicApi.Infrastructure.Repositories
{
    public class BetRepository : IBetRepository
    {
        private IFireStoreConnection _fireStoreConnection;
        public BetRepository(IFireStoreConnection fireStoreConnection)
        {
            _fireStoreConnection = fireStoreConnection;
        }

        public async Task<Bet> Create(Bet betData)
        {
            betData.Color = (!string.IsNullOrEmpty(betData.Color)) ? betData.Color.ToUpper() : "";
            if (IsValidBet(betData)) {
                Roulette rouletteData = await _fireStoreConnection.GetByID(betData.RouletteId);
                if (rouletteData != null && rouletteData.Status == Status.Open.ToString())
                {
                    betData.Id = Guid.NewGuid().ToString();
                    betData.CreatedAt = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
                    _fireStoreConnection.CreateBet(betData);
                    return betData;
                }
               
            }
            return null;
        }

        private bool IsValidBet(Bet betData) {
            if (!string.IsNullOrEmpty(betData.Color) && betData.Color != Colors.NEGRO.ToString() && betData.Color != Colors.ROJO.ToString())
                return false;

            if ((betData.Number < 0 || betData.Number > 36))
                return false;

            if (betData.Amount < 0 || betData.Amount > 10000) 
                return false;

            return true;
        }
    }
}
