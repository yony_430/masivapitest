﻿using AutoMapper;
using MasicApi.Core.Entities;
using MasicApi.Infrastructure.Data.Models;

namespace MasicApi.Infrastructure.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<RouletteFirestore, Roulette>();
            CreateMap<BetFirestore, Bet>();
            CreateMap<RouletteId, Roulette>();
        }
    }
}
