﻿using System;
using Google.Cloud.Firestore;

namespace MasicApi.Infrastructure.Data.Models
{
    [FirestoreData]
    public class RouletteFirestore
    {
        [FirestoreProperty]
        public string Id { get; set; }
        [FirestoreProperty]
        public string Status { get; set; }
        [FirestoreProperty]
        public string NumberWinner { get; set; }
        [FirestoreProperty]
        public string ColorWinner { get; set; }
        [FirestoreProperty]
        public DateTime CreatedAt { get; set; }
    }
}
