﻿using Google.Cloud.Firestore;
using System;

namespace MasicApi.Infrastructure.Data.Models
{
    [FirestoreData]
    public class BetFirestore
    {
        [FirestoreProperty]
        public string Id { get; set; }
        [FirestoreProperty]
        public string FirstName { get; set; }
        [FirestoreProperty]
        public string LastName { get; set; }
        [FirestoreProperty]
        public int Number { get; set; }
        [FirestoreProperty]
        public string Color { get; set; }
        [FirestoreProperty]
        public double Amount { get; set; }
        [FirestoreProperty]
        public double TotalWinner { get; set; }
        [FirestoreProperty]
        public string RouletteId { get; set; }
        [FirestoreProperty]
        public DateTime CreatedAt { get; set; }
    }
}
