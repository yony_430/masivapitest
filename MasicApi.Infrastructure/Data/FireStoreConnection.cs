﻿using AutoMapper;
using Google.Cloud.Firestore;
using MasicApi.Core.Entities;
using MasicApi.Core.Interfaces;
using MasicApi.Infrastructure.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MasicApi.Infrastructure.Data

{
    public class FireStoreConnection : IFireStoreConnection
    {
        private FirestoreDb _fireStoreDb;
        private string _pathConfiguration = AppDomain.CurrentDomain.BaseDirectory + @"roulette-api-firebase.json";
        private IMapper _mapper;
        public FireStoreConnection(IMapper mapper)
        {
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", _pathConfiguration);
            _fireStoreDb = FirestoreDb.Create("roulette-api");
            _mapper = mapper;
        }

        public bool CreateRoulette(Roulette dataRoulette)
        {
            try
            {
                DocumentReference rouletteDocument = _fireStoreDb.Collection("Roulettes").Document(dataRoulette.Id);
                Dictionary<string, object> rouletteData = new Dictionary<string, object>()
                {
                    {"Status", dataRoulette.Status },
                    {"CreatedAt", dataRoulette.CreatedAt }
                };
                rouletteDocument.SetAsync(rouletteData);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdateRoulette(Roulette dataRoulette)
        {
            try
            {
                DocumentReference rouletteDocument = _fireStoreDb.Collection("Roulettes").Document(dataRoulette.Id);
                Dictionary<string, object> rouletteData = new Dictionary<string, object>()
                {
                    {"Status", dataRoulette.Status }
                };
                rouletteDocument.UpdateAsync(rouletteData);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<Roulette> GetByID(string id)
        {
            try
            {
                DocumentReference rouletteDocument = _fireStoreDb.Collection("Roulettes").Document(id);
                DocumentSnapshot snapshot = await rouletteDocument.GetSnapshotAsync();
                if (snapshot.Exists)
                {
                    var rouletteData = snapshot.ConvertTo<RouletteFirestore>();

                    return _mapper.Map<Roulette>(rouletteData);
                }

                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool CreateBet(Bet betData)
        {
            try
            {
                DocumentReference betsCollection = _fireStoreDb.Collection("Roulettes").Document(betData.RouletteId).
                                                        Collection("Bets").Document(betData.Id);
                Dictionary<string, object> bet = new Dictionary<string, object>()
                {
                    {"FirstName", betData.FirstName },
                    {"LastName", betData.LastName },
                    {"Number", betData.Number },
                    {"Color", betData.Color },
                    {"Amount", betData.Amount },
                    {"TotalWinner", 0 },
                    {"CreatedAt", betData.CreatedAt }
                };
                var t = betsCollection.SetAsync(bet);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<IEnumerable<Bet>> GetBets(Roulette dataRoulette)
        {
            try {
                List<Bet> bets = new List<Bet>();
                CollectionReference betsCollection = _fireStoreDb.Collection("Roulettes").Document(dataRoulette.Id).Collection("Bets");
                QuerySnapshot snapshotBets = await betsCollection.GetSnapshotAsync();
                foreach (DocumentSnapshot documentBet in snapshotBets)
                {
                    BetFirestore betFirestore = documentBet.ConvertTo<BetFirestore>();
                    betFirestore.Id = documentBet.Id;
                    bets.Add(_mapper.Map<Bet>(betFirestore));
                }

                return bets;
            }
            catch (Exception e) {
                return null;
            }
        }

        public bool UpdateBet(Roulette dataRoulette, Bet dataBet)
        {
            try {
                DocumentReference betDocument = _fireStoreDb.Collection("Roulettes").Document(dataRoulette.Id).Collection("Bets").Document(dataBet.Id);
                Dictionary<string, object> rouletteData = new Dictionary<string, object>()
                {
                    {"TotalWinner", dataBet.TotalWinner }
                };
                betDocument.UpdateAsync(rouletteData);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdateResultRoulette(Roulette dataRoulette)
        {
            try
            {
                DocumentReference roulettesCollection = _fireStoreDb.Collection("Roulettes").Document(dataRoulette.Id);
                Dictionary<string, object> rouletteData = new Dictionary<string, object>()
                {
                    {"NumberWinner", dataRoulette.NumberWinner },
                    {"ColorWinner", dataRoulette.ColorWinner }
                };
                roulettesCollection.UpdateAsync(rouletteData);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<IEnumerable<Roulette>> GetRoulettes()
        {
            try
            {
                List<Roulette> roulettes = new List<Roulette>();
                CollectionReference roulettesCollection = _fireStoreDb.Collection("Roulettes");
                QuerySnapshot snapshotRoulettes = await roulettesCollection.GetSnapshotAsync();
                foreach (DocumentSnapshot documentRoulette in snapshotRoulettes)
                {
                    RouletteFirestore rouletteFirestore = documentRoulette.ConvertTo<RouletteFirestore>();
                    rouletteFirestore.Id = documentRoulette.Id;
                    roulettes.Add(_mapper.Map<Roulette>(rouletteFirestore));
                }

                return roulettes;
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}
